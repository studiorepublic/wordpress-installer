# Before using this, make sure to set these variables in your bash profile (below is CT's setup)

# export LOCALHOST_PATH="/Volumes/CT Dev/_localhost/"
# export BOILERPLATE_DB="wp_boilerplatev2"
# export BOILERPLATE_FOLDER="boilerplatev2"



#!/bin/bash

# Retrieve path from user's profile
PROFILE_PATH="$HOME/.bash_profile"
if [ -f "$HOME/.bashrc" ]; then
  PROFILE_PATH="$HOME/.bashrc"
fi
source "$PROFILE_PATH"

# Prompt for project name
read -p "Enter project name: " project_name

# Convert project name to lower case and replace spaces with underscores
theme_name=$project_name
project_name=$(echo "$project_name" | tr '[:upper:]' '[:lower:]' | tr ' ' '_')

# Duplicate database
mysql -u root -proot -e "CREATE DATABASE $project_name;"
mysqldump -u root -proot $BOILERPLATE_DB | mysql -u root -proot $project_name

# Copy folder
cp -r "$LOCALHOST_PATH/$BOILERPLATE_FOLDER" "$LOCALHOST_PATH/$project_name"

# Update wp-config.php
wp_config="$LOCALHOST_PATH/$project_name/wp-config.php"
sed -i '' "s/'DB_NAME', '.*'/'DB_NAME', '$project_name'/g" "$wp_config"
sed -i '' "s/'DB_USER', '.*'/'DB_USER', 'root'/g" "$wp_config"
sed -i '' "s/'DB_PASSWORD', '.*'/'DB_PASSWORD', 'root'/g" "$wp_config"

# Update siteurl and home options
mysql -u root -proot -e "USE $project_name; UPDATE wp_options SET option_value = 'http://$project_name.test/' WHERE option_name = 'siteurl';"
mysql -u root -proot -e "USE $project_name; UPDATE wp_options SET option_value = 'http://$project_name.test/' WHERE option_name = 'home';"

# Rename theme folder and create git repository
mv "$LOCALHOST_PATH/$project_name/wp-content/themes/boilerplatev3" "$LOCALHOST_PATH/$project_name/wp-content/themes/$project_name"
rm -rf "$LOCALHOST_PATH/$project_name/wp-content/themes/$project_name/.git"
cd "$LOCALHOST_PATH/$project_name/wp-content/themes/$project_name" && git init

# Change theme name in style.css
sed -i '' "s/SR Boilerplate v3/$theme_name/g" "$LOCALHOST_PATH/$project_name/wp-content/themes/$project_name/style.css"

# Activate new theme in WordPress
mysql -u root -proot -e "USE $project_name; UPDATE wp_options SET option_value = '$project_name' WHERE option_name = 'template';"
mysql -u root -proot -e "USE $project_name; UPDATE wp_options SET option_value = '$project_name' WHERE option_name = 'stylesheet';"

