#!/bin/bash -e

wpuser='sr'

clear

echo "    %@@@@@@@    @@@@@@@@@@@@@@@ &@@@@@@   @@@@@@@ &@@@@@@@@@         @@@@@@@      %@@@@@@&                              "
echo " &@@@@@@@@@@@@% @@@@@@@@@@@@@@@ &@@@@@@   @@@@@@@ &@@@@@@@@@@@@@%    @@@@@@@    @@@@@@@@@@@@&                           "
echo "@@@@@@@@%@@@@@@ @@@@@@@@@@@@@@@ &@@@@@@   @@@@@@@ &@@@@@@@@@@@@@@@   @@@@@@@   @@@@@@@&@@@@@@@                          "
echo "@@@@@@@   @@@@@@   %@@@@@@@     &@@@@@@   @@@@@@@ &@@@@@@  &@@@@@@%  @@@@@@@  @@@@@@@  @@@@@@@                          "
echo "@@@@@@@@% &        %@@@@@@@     &@@@@@@   @@@@@@@ &@@@@@@   @@@@@@@  @@@@@@@ &@@@@@@&   @@@@@@@                         "
echo " &@@@@@@@@&        %@@@@@@@     &@@@@@@   @@@@@@@ &@@@@@@   @@@@@@@  @@@@@@@ &@@@@@@%   @@@@@@@                         "
echo "   @@@@@@@@@@      %@@@@@@@     &@@@@@@   @@@@@@@ &@@@@@@   @@@@@@@@ @@@@@@@ &@@@@@@%   @@@@@@@                         "
echo "    %@@@@@@@@@%    %@@@@@@@     &@@@@@@   @@@@@@@ &@@@@@@   @@@@@@@  @@@@@@@ &@@@@@@%   @@@@@@@                         "
echo "    %@ &@@@@@@@    %@@@@@@@     &@@@@@@   @@@@@@@ &@@@@@@   @@@@@@@  @@@@@@@ &@@@@@@%   @@@@@@@                         "
echo "@@@@@@   @@@@@@@   %@@@@@@@     &@@@@@@   @@@@@@@ &@@@@@@  &@@@@@@@  @@@@@@@  @@@@@@@  %@@@@@@&                         "
echo "@@@@@@& %@@@@@@@   %@@@@@@@     &@@@@@@&&@@@@@@@& &@@@@@@&&@@@@@@@   @@@@@@@  &@@@@@@%%@@@@@@@                          "
echo " @@@@@@@@@@@@@&    %@@@@@@@      @@@@@@@@@@@@@@&  &@@@@@@@@@@@@@@    @@@@@@@   %@@@@@@@@@@@@&   (((((((((((((((((((((((("
echo "   @@@@@@@@@@      %@@@@@@@        &@@@@@@@@@@    &@@@@@@@@@@@%      @@@@@@@     @@@@@@@@@@     (((((((((((((((((((((((("
echo "                                                                                                                        "
echo "@@@@@@@@@@@@@    &@@@@@@@@@@@@@%@@@@@@@@@@@&    @@@@@@@   @@@@@@@&@@@@@@@@@@@@     &@@@@@@%     @@@@@@@      @@@@@@@@%  "
echo "@@@@@@@@@@@@@@@% &@@@@@@@@@@@@@%@@@@@@@@@@@@@@@ @@@@@@@   @@@@@@@&@@@@@@@@@@@@@@@% &@@@@@@%     @@@@@@@   &@@@@@@@@@@@@&"
echo "@@@@@@@%@@@@@@@@&&@@@@@@@%%%%%%%@@@@@@&%@@@@@@@%@@@@@@@   @@@@@@@&@@@@@@@%@@@@@@@@ &@@@@@@%     @@@@@@@  @@@@@@@@@@@@@@@"
echo "@@@@@@@   @@@@@@&&@@@@@@@      %@@@@@@%  &@@@@@@@@@@@@@   @@@@@@@&@@@@@@@  @@@@@@@ &@@@@@@%     @@@@@@@ &@@@@@@@  @@@@@@"
echo "@@@@@@@   @@@@@@&&@@@@@@@      %@@@@@@%  &@@@@@@@@@@@@@   @@@@@@@&@@@@@@@  @@@@@@@ &@@@@@@%     @@@@@@@ &@@@@@@%  &&&&  "
echo "@@@@@@@ &@@@@@@@ &@@@@@@@@@@@% %@@@@@@% %@@@@@@&@@@@@@@   @@@@@@@&@@@@@@@@@@@@@@&  &@@@@@@%     @@@@@@@ &@@@@@@%        "
echo "@@@@@@@@@@@@@@%  &@@@@@@@@@@@% %@@@@@@@@@@@@@@@%@@@@@@@   @@@@@@@&@@@@@@@@@@@@@@   &@@@@@@%     @@@@@@@ &@@@@@@%        "
echo "@@@@@@@@@@@@@@&  &@@@@@@@&&&&% %@@@@@@@@@@@@@&  @@@@@@@   @@@@@@@&@@@@@@@&@@@@@@@@ &@@@@@@%     @@@@@@@ &@@@@@@%        "
echo "@@@@@@@ &@@@@@@@ &@@@@@@@      %@@@@@@%         @@@@@@@   @@@@@@@&@@@@@@@   @@@@@@@&@@@@@@%     @@@@@@@ &@@@@@@%  @@@@@@"
echo "@@@@@@@  @@@@@@@ &@@@@@@@      %@@@@@@%         @@@@@@@   @@@@@@@ @@@@@@@   @@@@@@@&@@@@@@%     @@@@@@@  @@@@@@@  @@@@@@"
echo "@@@@@@@  @@@@@@@ &@@@@@@@&&&&&&%@@@@@@%          @@@@@@@&@@@@@@@@ @@@@@@@&@@@@@@@@&&@@@@@@&&&&&&@@@@@@@  &@@@@@@@@@@@@@@"
echo "@@@@@@@  @@@@@@@&&@@@@@@@@@@@@@%@@@@@@%          %@@@@@@@@@@@@@%  @@@@@@@@@@@@@@@% &@@@@@@@@@@@@@@@@@@@   %@@@@@@@@@@@@%"
echo "@@@@@@@   @@@@@@@@@@@@@@@@@@@@@%@@@@@@%             %@@@@@@@@     @@@@@@@@@@@%     &@@@@@@@@@@@@@@@@@@@      @@@@@@@&   "
echo ""
echo "================================================================="
echo "Studio Republic WordPress Installer"
echo "================================================================="

# generate random 12 character password
password=$(LC_ALL=C tr -dc A-Za-z0-9 < /dev/urandom | head -c 12)
# password= $(/dev/urandom | env LC_ALL=C tr -dc a-z1-9A-Z | head -c 10)

# copy password to clipboard
echo $password | pbcopy

# accept the name of our website
read -p "Theme Name - will be used for the theme, db, MAMP host and site names [test]: " themename
themename=${themename:-"test"}
echo $themename

mamphost=$themename."test"

# accept the theme repo
read -p "Theme Repo[git@bitbucket.org:studiorepublic/wordpress-boilerplate-theme-v3.git]: " themerepo
#themerepo=${themerepo:-"git@bitbucket.org:studiorepublic/wordpress-boilerplate-theme.git"}
themerepo=${themerepo:-"git@bitbucket.org:studiorepublic/wordpress-boilerplate-theme-v3.git"}
echo $themerepo;

# accept a comma separated list of pages
read -p "Add Pages[Our Work,Donate,Donate Thank You,News,Contact,Contact Thank You,Privacy and Legal,Site Map,Sample Content Page]: " allpages
allpages=${allpages:-"Our Work,Donate,Donate Thank You,News,Contact,Contact Thank You,Privacy and Legal,Site Map,Sample Content Page"}
echo $allpages



# add a simple yes/no confirmation before we proceed
echo "Run Install? (y/n)"
read -p "y" run

# if the user didn't say no, then go ahead an install
if [ "$run" == n ] ; then
exit
else




# download the WordPress core files
wp core download

# create the wp-config file with our standard setup
#wp core config --dbname=$themename --dbuser=root --dbpass=root --dbhost=127.0.0.1:8889 --extra-php <<PHP
#define( 'DISALLOW_FILE_EDIT', true );
#define( 'WP_DEBUG_DISPLAY', true );
#define( 'ERROR_REPORTING', true );
#PHP

# create the wp-config file with our standard setup
wp core config --dbname=$themename --dbuser=root --dbpass=root --dbhost=127.0.0.1:8889
wp config set --raw WP_DEBUG true
wp config set --raw DISALLOW_FILE_EDIT true
wp config set --raw WP_DEBUG_DISPLAY true
wp config set --raw ERROR_REPORTING true

# parse the current directory name
currentdirectory=${PWD##*/}

# create database, and install WordPress
wp db create

wp core install --url="$mamphost" --title="$themename" --admin_user="$wpuser" --admin_email="devteam@studiorepublic.com"  --admin_password="$password"

wp role create SR StudioRepublic --clone=administrator
wp user add-role sr SR

# delete sample page, and create homepage
wp post delete $(wp post list --post_type=page --posts_per_page=1 --post_status=publish --pagename="sample-page" --field=ID --format=ids)
wp post create --post_type=page --post_title=Home --post_status=publish

# create all of the pages
export IFS=","
for page in $allpages; do
	wp post create --post_type=page --post_status=publish --post_title="$(echo $page | sed -e 's/^ *//' -e 's/ *$//')"
done

# set homepage as front page
wp option update show_on_front 'page'

# set homepage to be the new page
wp option update page_on_front $(wp post list --post_type=page --post_status=publish --posts_per_page=1 --pagename=home --field=ID --format=ids)


# set pretty urls
wp rewrite structure '/%postname%/' --hard
wp rewrite flush --hard

# delete akismet and hello dolly
wp plugin delete akismet
wp plugin delete hello

#install dotenv
composer require vlucas/phpdotenv

# install  plugins

# # install acf
# # get plugin path
acf_zip_file="acf-pro.zip"
curl 'https://connect.advancedcustomfields.com/index.php?p=pro&a=download&k=b3JkZXJfaWQ9MTAyMjA0fHR5cGU9ZGV2ZWxvcGVyfGRhdGU9MjAxNy0wMy0xNyAxMjowNDowMA==' --output ${acf_zip_file}
wp plugin install ${acf_zip_file}
wp plugin activate advanced-custom-fields-pro
rm ${acf_zip_file}
wp eval 'acf_pro_update_license("b3JkZXJfaWQ9MTAyMjA0fHR5cGU9ZGV2ZWxvcGVyfGRhdGU9MjAxNy0wMy0xNyAxMjowNDowMA==");'
git clone https://github.com/hoppinger/advanced-custom-fields-wpcli.git wp-content/plugins/advanced-custom-fields-wpcli
wp plugin activate advanced-custom-fields-wpcli
wp plugin install gravityformscli --activate
wp gf install --key=8086371519ff3e78f6999d3e007d424d --activate
wp plugin install timber-library --activate
wp plugin install wordpress-seo --activate
wp plugin install acf-gravityforms-add-on --activate
wp plugin install mainwp-child 
wp plugin install redirection --activate
# wp plugin install wp-mail-smtp --activate
wp plugin install login-as-user --activate
wp plugin install duplicate-page --activate


wp theme delete --all --force

# install the boilerplate theme
git clone $themerepo wp-content/themes/$themename/
rm wp-content/themes/$themename/style.css
printf '/*\nTheme Name: '$themename'\nAuthor: Studio Republic\nAuthor URI: https://www.studiorepublic.com\nDescription:\nVersion: 1.0\nLicense: GNU General Public License v2 or later\n*/\n' > wp-content/themes/$themename/style.css
rm -Rf wp-content/themes/$themename/.git
git init wp-content/themes/$themename/

wp theme activate "$themename"

clear

# create a navigation bar
wp menu create "Main Navigation"

# add pages to navigation
export IFS=" "
for pageid in $(wp post list --order="ASC" --orderby="date" --post_type=page --post_status=publish --posts_per_page=-1 --field=ID --format=ids); do
	wp menu item add-post main-navigation $pageid
done

# assign navigation to primary location
wp menu location assign main-navigation main

clear
echo "================================================================="
echo "Installation is complete. Your username/password is listed below."
echo ""
echo "Username: $wpuser"
echo "Password: $password (in your clipboard)"
echo ""
echo "================================================================="

# Open the new website with Google Chrome
/usr/bin/open -a "/Applications/Google Chrome.app" "${mamphost}"

fi
